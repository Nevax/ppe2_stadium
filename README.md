# PPE2 Stadium

- [ ] Distribution des tâches
- [ ] Mission 1 :  Découverte des CMS
    - [x] Définition CMS
    - [x] Tableau comparatif
    - [x] Récapituler les principaux critères de chois (10 max.)
    - [x] Rechercher les plugins
    - [ ] Comparer les CMS choisi
        - [x] Analyser les structures de l'afficahe
        - [x] Observer les menus & info présent
        - [ ] Noter la hirarchie des pages nécéssaires
    - [ ] Choisir un CMS avec note structurée 1 page
    - [ ] Ebauche du site vitrine
    - [ ] Schémas et maquettes
- [ ] Mission n°2 : CREATION DU SITE WEB VITRINE
    - [ ]

## Mission n°1 : Découverte des CMS
### Définir la notion de CMS

Un CMS (ou Content Management System) est un outil permettant de mettre à jour dynamiquement un site web. Voici une présentation du fonctionnement d’un C. M. S.
Il permet d’éditer toutes sortes de contenus. L’édition de ces contenus est assez simple grâce à une interface graphique de mise en forme à l’image des traitements de textes ainsi que de balises simplifiées comme le BBCode ou le Markdown.
Les CMS permettent la mise en place de contenu simplifié tout comme la prise en main. Cependant, pour des demandes spécifiques, il faut rapidement rajouter des plugins ou faire des développements spécifiques.

### Étudier le tableau comparatif suivant
    http://socialcompare.com/fr/comparison/popular-content-management-system-cms-comparison-table

### Récapituler les principaux critères de choix (10 maximum)

|  Critères                 |  Wordpress   |  Xoops    |
|---------------------------| :----------: | :-------: |
| SSL Compatible            | OK           | OK        |
| Captcha                   | KO Plugin    | KO Plugin |
| Abonnement                | KO Plugin    | KO Plugin |
| Flux RSS                  | OK           | OK        |
| Plan du Site              | KO Plugin    | KO Plugin |
| Support Commercial        | OK           | OK        |
| Statistiques Web          | KO Plugin    | KO Plugin |
| Gallerie photo            | OK           | KO Plugin |
| Gestion des évènements    | KO Plugin    | KO Plugin |
| Gestion FAQ               | OK/KO Plugin | OK        |
| Historique des Connexions | KO Plugin    | KO Plugin |

Nous avons tout d'abord choisi 2 CMS parmi les 12 proposés. Il nous a semblé pertinent de confronter 2 CMS avec  10 critères de choix. Les 2 CMS choisis sont WordPress et Xoops et se confrontent avec les critères suivants :
SSL Compatible, Ce critère a été sélectionné car la sécurisation des échanges sur internet est importante, plus précisément pour le site boutique et pour l'authentification des utilisateurs.

* Captcha, Ce critère permet de différencier de manière automatisé un utilisateur humain d'un ordinateur, par la présence de CAPTCHA, le site sera protégé des inscriptions ,connexions etc.. effectués par des ordinateurs.
Abonnement, ce critère est quant à lui aussi intéressant et important car il permet aux utilisateurs de s'abonner à la newsletter du site afin d'être mis au courant des diverses nouvelles ou modifications.

* Flux RSS, Le flux RSS permet de produire automatiquement du contenu d'un fichier texte particulier en fonction des mises à jours d'un site web.

* Plan du Site, Ce critère est important puisqu'il permet aux utilisateurs de jouir d'une bonne ergonomie et d’une facilité d'utilisation du site si celui ci a un bon plan.

* Support Commercial : Ce critère sera utile si l'on veut vendre les différents produits en rapport avec le stade directement sur leur site.

* Statistiques Web : Ce critère sera utile afin de voir le nombre de connexion, les achats effectués,etc …

* Gallerie photo : Ce critère est très important afin de partager les photos du stade et des différents matchs.

* Gestion des évènements : Celui- ci permettra de prévenir les utilisateurs de voir les événements à venir qui auront  lieu dans le stade .

* Gestion FAQ : Ce critère est important afin de permettre aux utilisateurs de poser des questions directement via le site.

* Historique des Connexions : Ce critère permettra de voir quels utilisateurs se connecte, afin de lui proposer une meilleure visite en ciblant les recherches qu’ils ont déjà effectuées, de faire un site personnalisé pour l’utilisateur.


### Rechercher les plugins nécessaires à la réalisation du site.

|  Fonction                 |  Plugin                                                                             |
|---------------------------| :---------------------------------------------------------------------------------: |
| Captcha                   | [Captcha par BestWebSoft](https://fr.wordpress.org/plugins/captcha/)                |
| Abonnement                | [WP Subscription](https://fr.wordpress.org/plugins/wp-subscription/)                |
| Plan du site              | [WP Sitemap Page](https://fr.wordpress.org/plugins/wp-sitemap-page/)                |
| Statistique               | [WP Statistics](https://wordpress.org/plugins/wp-statistics/)                       |
| Historique de connexion   | [Login Security Solution](https://fr.wordpress.org/plugins/login-security-solution/)|
| Gestionnaire d'événement  | [Events Manager](https://fr.wordpress.org/plugins/events-manager/)                  |
| Géstionnaire de FAQ       | [WP Awesome FAQ Plugin](https://fr.wordpress.org/plugins/wp-awesome-faq/)           |
| Carte                     | [WP Google Maps](https://fr.wordpress.org/plugins/wp-google-maps/)                  |

### Analysant la structure de l’affichage proposé par ceux-ci

#### Page Administrateur Démo de WordPress

* Analysant la structure de l’affichage proposé par ceux-ci

##### Page Job Board

![alt text](img/ui_cms/wp1_main.png)

Barre de menu contenant 4 rubriques (Dashboard, Job board, Profile, Settings)

`BLEU` Catégorie de chaque colonne

`MARRON` Barre de recherche

`JAUNE` Liste des jobs disponibles

`VERT` Zone de connexion utilisateur

Menu déroulant d'action possible sur les jobs sélectionnés et filtre des jobs       disponibles en menu déroulant

* Observant les menus et informations présents (orientation des menus, cadres, etc.)

##### Dans le menu déroulant "Job board"

![alt text](img/ui_cms/wp2.png)

Dans la rubrique "Applications", liste des personnes ayant créé une application sur le site.

Dans la rubrique "Employers", liste des postes proposés par des employeurs

Dans la rubrique "Candidates", liste des candidats du site

Dans la rubrique "Payments", liste des paiements effectués sur le site

Dans la rubrique "Membership", liste des adhésions et des adhérents

Dans la rubrique "E mail alerts", liste des alertes e-mail

##### Dans le menu "Profile"

![alt text](img/ui_cms/wp3_profile.png)

Page permettant de rentrer les informations personnelles des utilisateurs ainsi que le choix des couleurs du théme.


Dans le menu "Settings"

![alt text](img/ui_cms/wp4.png)

Dans la rubrique "Configuration", possibilité de configurer toute les options du sites. Il y'a une partie configuration, intégrations, payment methods and others.

Dans la rubrique "Pricing", possibilité d'ajouter des postes, résumés et des membres employeurs.

Dans la rubrique "Custom Fields", il est possible de modifier des champs.

Dans la rubrique "Promotions", il est possible d'ajouter une promotion pour un poste ou un membre employeur.

Dans la rubrique "Categories", liste des catégories des métiers avec la référence du nombre de métier disponible, dans chaque catégories.

Dans la rubrique "Job types", liste des types de métiers avec le nombre total de métier disponibles dans chaque types.

Dans la rubrique "Email Templates", il est possible de donner un titre pour plusieurs cas d'alerte quand il y'a du nouveau sur le site ( comme un nouveau job qui a été posté).

*Dans la rubrique "Import/Export", il y'a la possibilité d'importer un fichier XML, CSV et d'exporter des fichiers XML.

Notant la hiérarchie des pages nécessaires (plan, mentions légales, …)

#### Xoops :

Dans la partie menu administration il y a une  barre de menu comportant quatre rubriques (Control Pannel Home, Modules, Préférences, Links)

les différente catégorie sont :

![alt text](img/ui_cms/xoops1.png)

Control Pannel Home : (Home page, Xoops news, Logout) cette rubrique permet d'aller sur la page principale de Xoops, de voir les nouveauté ou de se déconnecter.

![alt text](img/ui_cms/xoops2.png)

Modules : ( Systeme :  avatars, banners, blocks comments, groups, image manager, email Users, Maintemance, modules,preferences, smilies, tremplate, user ranks et user ) celle-ci permet d'accerder au different module proposer par Xoops.

![alt text](img/ui_cms/xoops3.png)

Preferences : ((System options : general settings, user info settings, meta tags and footer, word censoring options, search options, email setup, authentification options, system module setings), system) cette rubrique sert a utiliser les option du systeme Xoops,

![alt text](img/ui_cms/xoops4.png)

Links : (Xoops projects, community site, XOOPS modules, Xoops themes) enfin cette rubrique permet d'acceder au projets de la commumauter Xoops, ainsi que les different themes.


### Note à la direction

>>>
De : Javatar

ESN française situé à Valence

                                                                                                 A La Direction de Stadium Company


                                                       Note à la direction


Objet : Choix du CMS pour le site vitrine de Stadium Company

Stadium Company doit faire une présentation générale de sa société. Il a été décidé d'effectuer cette présentation à l'aide d'un site web vitrine construit à partir d'un CMS, afin de faciliter la création du site web.

Or, pour garantir la qualité du site vitrine travail lors de la présentation, nous estimons que le CMS Wordpress semble le plus adapté pour le cahier des charges présenté par Stadium Company.

La présentation du site vitrine doit d’être un point fort de l'entreprises Stadium Company. Nous avons donc retenu deux CMS avec lesquels nous aurions réalisé le site vitrine répondant le plus aux critères donnés par Stadium Company. Les deux CMS retenus étaient Wordpress et Xoops et, après les avoir essayés et comparé, nous avons sélectionné Wordpress pour différentes raisons.

Wordpress est un CMS commun, multifonction, idéal pour la création de site vitrine et facilement maniable. Comparé aux autres CMS, Wordpress est le CMS offrant le plus grand nombre de plugins et est le plus utilisé dans le monde (30%). Wordpress sera facilement adaptable au cahier des charges présenté par Stadium Company et le site vitrine pourra être fait en un temps optimal avec la qualité recherchée.

La création de ce site web vitrine permettra l’évolution future de l'entreprise. Cela permettra à Stadium Company de faire connaître mondialement via le web et lui permettra surement de deavancer ses concurrents. De plus, la société aura la possibilité d'aborder de nouveaux projets plus dirigé vers le web qui est en pleine extension. Cet avantage considérable lui permet donc d’avoir beaucoup plus d’impact et de pouvoir couvrir beaucoup plus facilement tous ses clients dans les différentes zones géographiques. Et cette facilité de pouvoir couvrir ces étendues doit être optimisée par ce site web vitrine lors de la présentation générale.

Par ces points, il me semble plus que nécessaire de créer une homogénéisation des équipements pour tous les visiteurs médicaux.

                                                                   Je vous prie d’agréer Madame, Monsieur à mes sincères salutations

                                                                                                                    Martin Guilhaume
>>>

### Maquette/schéma/carte heuristique

Maquette du site :
![alt text](img/maquette_stadium.png)

Schéma du site :
![alt text](img/schema_Stadium.png)

Carte heuristique :
![alt text](img/carte_heristique.png)


## Mission n°2 : CREATION DU SITE WEB VITRINE

### Un système opérationnel
### Un compte rendu rédigé en français présentant obligatoirement :
### Les retours d’expérience, la conduite de projet avec une répartition des tâches,
### Un tableau comparatif structuré des principaux CMS.
### Une note structurée sur le choix parmi deux CMS, que vous adressez à la direction
### Une documentation sur le site développé : maquettes, carte heuristique, installation de plugs-in et modules, ajout de contenus (catégories, articles…), modification de thèmes, templates, modification de l’aspect du site…
### Une documentation technique sur l’installation et le paramétrage de l’outil

### Un diaporama EN ANGLAIS présentant votre réalisation (organisation, choix techniques…)

## Prestations orales
